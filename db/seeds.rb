# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#Setting.create!(name: 'main_warehouse_id', value: '1', kind: 'Integer')

User.create!(username: 'admin', firstname: '', lastname: '', email: 'admin@admin.com', password:'passwordd', admin: true, sign_in_count: 1, current_sign_in_at:'2016-10-03 08:42:23.951841', last_sign_in_at:'2016-10-03 08:37:24.108795', current_sign_in_ip:'127.0.0.1', last_sign_in_ip:'127.0.0.1', failed_attempts:0, created_at:'2016-10-03 08:37:23.918309',updated_at:'2016-10-03 08:42:23.958757')

User.create!(username: 'user', firstname: '', lastname: '', email: 'user1@user.com', password:'passwordd', sign_in_count: 1, current_sign_in_at:'2016-10-03 08:42:23.951841', last_sign_in_at:'2016-10-03 08:37:24.108795', current_sign_in_ip:'127.0.0.1', last_sign_in_ip:'127.0.0.1', failed_attempts:0, created_at:'2016-10-03 08:37:23.918309',updated_at:'2016-10-03 08:42:23.958757')



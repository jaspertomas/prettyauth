class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table(:users) do |t|
      ## Database authenticatable
      t.string :username,           null: false, default: ""
      t.string :firstname,          null: false, default: ""
      t.string :lastname,           null: false, default: ""

      t.string :omb_id_no,           null: false, default: ""
      t.integer :gender,              null: true, default: ""
      t.integer :civil_status,              null: true, default: ""

      t.string :mobile,              null: true, default: ""
      t.string :landline,              null: true, default: ""
      t.string :landline_local,              null: true, default: ""
      t.string :email,              null: true, default: ""
      t.integer :age,              null: true, default: 0
      
      t.date :birthdate,              null: true, default: ""

      t.boolean :socmed_fb,              null: true, default: false
      t.boolean :socmed_ig,              null: true, default: false
      t.boolean :socmed_tw,              null: true, default: false
      t.boolean :socmed_vi,              null: true, default: false
      t.string :socmed_other,              null: true, default: ""

      t.string :position,              null: true, default: ""
      t.string :appointment_status,              null: true, default: ""
      t.string :rank,              null: true, default: ""
      t.string :development_partner,              null: true, default: ""
      t.integer :omb_years,              null: true, default: 0
      t.integer :bureau_years,              null: true, default: 0
      t.string :supervisor,              null: true, default: ""
      t.string :supervisor_position,              null: true, default: ""
      t.string :profession,              null: true, default: ""
      t.integer :salary_grade,              null: true, default: ""
      t.decimal :current_salary,              null: true, default: 0
      t.decimal :allowances,              null: true, default: 0
      t.boolean :is_lawyer,              null: true, default: false

      t.boolean :admin,            null: false, default: false
      t.boolean :active,            null: false, default: true

      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      t.string   :unlock_token # Only if unlock strategy is :email or :both
      t.datetime :locked_at


      t.timestamps null: false
    end

    add_index :users, :username,                unique: true
    #add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170326060403) do

  create_table "admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "username",               default: "",   null: false
    t.string   "firstname",              default: "",   null: false
    t.string   "lastname",               default: "",   null: false
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.boolean  "active",                 default: true, null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,    null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "value"
    t.string   "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_settings_on_name", unique: true, using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "username",                              default: "",    null: false
    t.string   "firstname",                             default: "",    null: false
    t.string   "lastname",                              default: "",    null: false
    t.string   "omb_id_no",                             default: "",    null: false
    t.integer  "gender"
    t.integer  "civil_status"
    t.string   "mobile",                                default: ""
    t.string   "landline",                              default: ""
    t.string   "landline_local",                        default: ""
    t.string   "email",                                 default: ""
    t.integer  "age",                                   default: 0
    t.date     "birthdate"
    t.boolean  "socmed_fb",                             default: false
    t.boolean  "socmed_ig",                             default: false
    t.boolean  "socmed_tw",                             default: false
    t.boolean  "socmed_vi",                             default: false
    t.string   "socmed_other",                          default: ""
    t.string   "position",                              default: ""
    t.string   "appointment_status",                    default: ""
    t.string   "rank",                                  default: ""
    t.string   "development_partner",                   default: ""
    t.integer  "omb_years",                             default: 0
    t.integer  "bureau_years",                          default: 0
    t.string   "supervisor",                            default: ""
    t.string   "supervisor_position",                   default: ""
    t.string   "profession",                            default: ""
    t.integer  "salary_grade"
    t.decimal  "current_salary",         precision: 10, default: 0
    t.decimal  "allowances",             precision: 10, default: 0
    t.boolean  "is_lawyer",                             default: false
    t.boolean  "admin",                                 default: false, null: false
    t.boolean  "active",                                default: true,  null: false
    t.string   "encrypted_password",                    default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",                       default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_users_on_username", unique: true, using: :btree
  end

end

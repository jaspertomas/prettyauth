class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable, :lockable 


  validates :username, uniqueness: true, presence: true
  validates :age, :numericality => {:only_integer => true}
#  validates :omb_years, :numericality => {:only_integer => true}
#  validates :bureau_years, :numericality => {:only_integer => true}
  validates :current_salary, :numericality => {:only_integer => true}
  validates :allowances, :numericality => {:only_integer => true}
  
  enum gender: [ "Male", "Female" ]
  enum civil_status: [ "Single", "Married", "Widowed", "Divorced", "Separated" ]
  enum salary_grade: (0..30)

  def active_for_authentication?
    super && self.active?
  end

  def inactive_message
    "Sorry, this account has been deactivated."
  end
  def email_required?
    false
  end
end

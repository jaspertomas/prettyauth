json.extract! user, :id, :username, :firstname, :lastname, :omb_id_no, :gender, :civil_status, :mobile, :landline, :landline_local, :email, :age, :birthdate, :socmed_fb, :socmed_ig, :socmed_tw, :socmed_vi, :socmed_other, :admin, :password, :password_confirmation, :active, :created_at, :updated_at
json.url user_url(user, format: :json)

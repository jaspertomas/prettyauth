json.extract! setting, :id, :name, :value, :kind, :created_at, :updated_at
json.url setting_url(setting, format: :json)
class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authorize_action
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # GET /users
  # GET /users.json
  def index
    @users = User.all.paginate(page: params[:page], per_page: Constants::RECORDS_PER_PAGE)
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    #if current user is trying to disable himself  
    if(current_user.id==@user.id and user_params['active']=='0')
      return redirect_to :back, notice: 'You cannot deactivate yourself'
    end
  
    #if password is empty, don't change password
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
    end
  
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    #if current user is trying to delete himself  
    if(current_user.id==@user.id)
      #soft delete
      #return redirect_to :back, notice: 'You cannot delete yourself'
      return redirect_to :back, notice: 'You cannot deactivate yourself'
    end
  
    #soft delete
    #@user.destroy
    @user.update(active: 0)
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully deactivated.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params[:user][:salary_grade]=params[:user][:salary_grade].to_i
      params[:user][:omb_years]=params[:user][:omb_years].to_i
      params[:user][:bureau_years]=params[:user][:bureau_years].to_i

      params.require(:user).permit(:username, :firstname, :lastname, :omb_id_no, :gender, :civil_status, :mobile, :landline, :landline_local, :email, :age, :birthdate, :socmed_fb, :socmed_ig, :socmed_tw, :socmed_vi, :socmed_other, :admin, :password, :password_confirmation, :active, :position, :appointment_status, :rank, :development_partner, :omb_years, :bureau_years, :supervisor, :supervisor_position, :profession, :salary_grade, :current_salary, :allowances, :is_lawyer )
    end
    
    def user_not_authorized
      flash[:warning] = "You are not authorized to perform this action."
      redirect_to(request.referrer || root_path)
    end
    
    def authorize_action
      authorize User
    end
end

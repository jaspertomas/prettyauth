class HomeController < ApplicationController

  before_action :authenticate_user!, only: [:restricted]

  def index
  end

  def help
  end

  def contact
  end
  
  #this is a general purpose error page
  #usage: 
  #return redirect_to home_error_path+"?msg=blahblah" if error_condition
  def error
    @msg=params[:msg]
    respond_to do |format|
      format.html { render :layout => !request.xhr? }
    end
  end
end

